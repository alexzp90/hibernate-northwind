package com.example.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "ORDERS", indexes = {
        @Index(name = "customer_id", columnList = "customer_id"),
        @Index(name = "employee_id", columnList = "employee_id"),
        @Index(name = "shipper_id", columnList = "shipper_id"),
        @Index(name = "tax_status", columnList = "tax_status_id"),
        @Index(name = "ship_zip_postal_code", columnList = "ship_zip_postal_code"),
})
public class Order extends BaseEntity<Integer> {
    private static final long serialVersionUID = 8152231070628805614L;

    @Column(name = "employee_id")
    private Long employeeId;
    @ManyToOne(fetch = FetchType.LAZY, cascade = {
            CascadeType.ALL
    })
    @JoinColumn(name = "customer_id")
    private Customer customer;
    @Column
    private LocalDateTime orderDate;
    @Column
    private LocalDateTime shippedDate;
    @Column(name = "shipper_id")
    private Long shipperId;
    @Column(length = 50)
    private String shipName;
    @Lob
    @Column
    private String shipAddress;
    @Column(length = 50)
    private String shipCity;
    @Column(length = 50)
    private String shipStateProvince;
    @Column(name = "ship_zip_postal_code", length = 50)
    private String shipZipPostalCode;
    @Column(length = 50)
    private String shipCountryRegion;
    @Column(precision = 19, scale = 4)
    private BigDecimal shippingFee;
    @Column(precision = 19, scale = 4)
    private BigDecimal taxes;
    @Column(length = 50)
    private String paymentType;
    @Column
    private LocalDateTime paidDate;
    @Lob
    @Column
    private String notes;
    @Column
    private Double taxRate;
    @Column(name = "tax_status_id")
    private Byte taxStatusId;
    @Column
    private Byte statusId;
    @Column
    @Version
    private Integer version;

    public Order() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public LocalDateTime getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(LocalDateTime shippedDate) {
        this.shippedDate = shippedDate;
    }

    public Long getShipperId() {
        return shipperId;
    }

    public void setShipperId(Long shipperId) {
        this.shipperId = shipperId;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipStateProvince() {
        return shipStateProvince;
    }

    public void setShipStateProvince(String shipStateProvince) {
        this.shipStateProvince = shipStateProvince;
    }

    public String getShipZipPostalCode() {
        return shipZipPostalCode;
    }

    public void setShipZipPostalCode(String shipZipPostalCode) {
        this.shipZipPostalCode = shipZipPostalCode;
    }

    public String getShipCountryRegion() {
        return shipCountryRegion;
    }

    public void setShipCountryRegion(String shipCountryRegion) {
        this.shipCountryRegion = shipCountryRegion;
    }

    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    public BigDecimal getTaxes() {
        return taxes;
    }

    public void setTaxes(BigDecimal taxes) {
        this.taxes = taxes;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public LocalDateTime getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(LocalDateTime paidDate) {
        this.paidDate = paidDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Byte getTaxStatusId() {
        return taxStatusId;
    }

    public void setTaxStatusId(Byte taxStatusId) {
        this.taxStatusId = taxStatusId;
    }

    public Byte getStatusId() {
        return statusId;
    }

    public void setStatusId(Byte statusId) {
        this.statusId = statusId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(employeeId, order.employeeId) &&
                Objects.equals(customer, order.customer) &&
                Objects.equals(orderDate, order.orderDate) &&
                Objects.equals(shippedDate, order.shippedDate) &&
                Objects.equals(shipperId, order.shipperId) &&
                Objects.equals(shipName, order.shipName) &&
                Objects.equals(shipAddress, order.shipAddress) &&
                Objects.equals(shipCity, order.shipCity) &&
                Objects.equals(shipStateProvince, order.shipStateProvince) &&
                Objects.equals(shipZipPostalCode, order.shipZipPostalCode) &&
                Objects.equals(shipCountryRegion, order.shipCountryRegion) &&
                Objects.equals(shippingFee, order.shippingFee) &&
                Objects.equals(taxes, order.taxes) &&
                Objects.equals(paymentType, order.paymentType) &&
                Objects.equals(paidDate, order.paidDate) &&
                Objects.equals(notes, order.notes) &&
                Objects.equals(taxRate, order.taxRate) &&
                Objects.equals(taxStatusId, order.taxStatusId) &&
                Objects.equals(statusId, order.statusId) &&
                Objects.equals(version, order.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeId, customer, orderDate, shippedDate, shipperId, shipName, shipAddress, shipCity, shipStateProvince, shipZipPostalCode, shipCountryRegion, shippingFee, taxes, paymentType, paidDate, notes, taxRate, taxStatusId, statusId, version);
    }

    @Override
    public String toString() {
        return "Order{" +
                "employeeId=" + employeeId +
                ", customer=" + customer +
                ", orderDate=" + orderDate +
                ", shippedDate=" + shippedDate +
                ", shipperId=" + shipperId +
                ", shipName='" + shipName + '\'' +
                ", shipAddress='" + shipAddress + '\'' +
                ", shipCity='" + shipCity + '\'' +
                ", shipStateProvince='" + shipStateProvince + '\'' +
                ", shipZipPostalCode='" + shipZipPostalCode + '\'' +
                ", shipCountryRegion='" + shipCountryRegion + '\'' +
                ", shippingFee=" + shippingFee +
                ", taxes=" + taxes +
                ", paymentType='" + paymentType + '\'' +
                ", paidDate=" + paidDate +
                ", notes='" + notes + '\'' +
                ", taxRate=" + taxRate +
                ", taxStatusId=" + taxStatusId +
                ", statusId=" + statusId +
                ", version=" + version +
                '}';
    }
}
