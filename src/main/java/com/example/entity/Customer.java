package com.example.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "CUSTOMERS", indexes = {
        @Index(name = "city", columnList = "city"),
        @Index(name = "company", columnList = "company"),
        @Index(name = "first_name", columnList = "first_name"),
        @Index(name = "last_name", columnList = "last_name"),
        @Index(name = "zip_postal_code", columnList = "zip_postal_code"),
        @Index(name = "state_province", columnList = "state_province"),
})
public class Customer extends BaseEntity<Integer> {
    private static final long serialVersionUID = -5329195759652530580L;
    @Column(length = 50)
    private String company;
    @Column(name = "first_name", length = 50)
    private String firsName;
    @Column(name = "last_name", length = 50)
    private String lastName;
    @Column(length = 50)
    private String emailAddress;
    @OneToMany(mappedBy = "customer", cascade = {
            CascadeType.REFRESH,
            CascadeType.MERGE,
            CascadeType.REMOVE
    })
    private Set<Order> order;
    @Column(length = 50)
    private String jobTitle;
    @Column(length = 25)
    private String businessPhone;
    @Column(length = 25)
    private String homePhone;
    @Column(length = 25)
    private String mobilePhone;
    @Column(length = 25)
    private String faxPhone;
    @Lob
    @Column
    private String address;
    @Column
    private String city;
    @Column(name = "state_province", length = 50)
    private String stateProvince;
    @Column(name = "zip_postal_code", length = 15)
    private String zipPostalCode;
    @Column(length = 50)
    private String countryRegion;
    @Lob
    @Column
    private String webPage;
    @Lob
    @Column
    private String notes;
    @Column
    private byte[] attachments;
    @Column
    @Version
    private Integer version;

    public Customer() {
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirsName() {
        return firsName;
    }

    public void setFirsName(String firsName) {
        this.firsName = firsName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getFaxPhone() {
        return faxPhone;
    }

    public void setFaxPhone(String faxPhone) {
        this.faxPhone = faxPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public String getZipPostalCode() {
        return zipPostalCode;
    }

    public void setZipPostalCode(String zipPostalCode) {
        this.zipPostalCode = zipPostalCode;
    }

    public String getCountryRegion() {
        return countryRegion;
    }

    public void setCountryRegion(String countryRegion) {
        this.countryRegion = countryRegion;
    }

    public String getWebPage() {
        return webPage;
    }

    public void setWebPage(String webPage) {
        this.webPage = webPage;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public byte[] getAttachments() {
        return attachments;
    }

    public void setAttachments(byte[] attachments) {
        this.attachments = attachments;
    }

    public Set<Order> getOrder() {
        return order;
    }

    public void setOrder(Set<Order> order) {
        this.order = order;
    }

    public void addOrder(Order order) {
        this.order.add(order);
        order.setCustomer(this);
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(company, customer.company) &&
                Objects.equals(firsName, customer.firsName) &&
                Objects.equals(lastName, customer.lastName) &&
                Objects.equals(emailAddress, customer.emailAddress) &&
                Objects.equals(order, customer.order) &&
                Objects.equals(jobTitle, customer.jobTitle) &&
                Objects.equals(businessPhone, customer.businessPhone) &&
                Objects.equals(homePhone, customer.homePhone) &&
                Objects.equals(mobilePhone, customer.mobilePhone) &&
                Objects.equals(faxPhone, customer.faxPhone) &&
                Objects.equals(address, customer.address) &&
                Objects.equals(city, customer.city) &&
                Objects.equals(stateProvince, customer.stateProvince) &&
                Objects.equals(zipPostalCode, customer.zipPostalCode) &&
                Objects.equals(countryRegion, customer.countryRegion) &&
                Objects.equals(webPage, customer.webPage) &&
                Objects.equals(notes, customer.notes) &&
                Arrays.equals(attachments, customer.attachments) &&
                Objects.equals(version, customer.version);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(company, firsName, lastName, emailAddress, order, jobTitle, businessPhone, homePhone, mobilePhone, faxPhone, address, city, stateProvince, zipPostalCode, countryRegion, webPage, notes, version);
        result = 31 * result + Arrays.hashCode(attachments);
        return result;
    }

    @Override
    public String toString() {
        return "Customer{" +
                super.toString() + ", " +
                "company='" + company + '\'' +
                ", firsName='" + firsName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", order=" + order +
                ", jobTitle='" + jobTitle + '\'' +
                ", businessPhone='" + businessPhone + '\'' +
                ", homePhone='" + homePhone + '\'' +
                ", mobilePhone='" + mobilePhone + '\'' +
                ", faxPhone='" + faxPhone + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", stateProvince='" + stateProvince + '\'' +
                ", zipPostalCode='" + zipPostalCode + '\'' +
                ", countryRegion='" + countryRegion + '\'' +
                ", webPage='" + webPage + '\'' +
                ", notes='" + notes + '\'' +
                ", attachments=" + Arrays.toString(attachments) +
                ", version=" + version +
                '}';
    }
}
