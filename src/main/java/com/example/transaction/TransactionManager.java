package com.example.transaction;

import com.example.util.HibernateUtil;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.util.function.Consumer;
import java.util.function.Function;

public class TransactionManager {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(HibernateUtil.class.getSimpleName());
    private final EntityManagerFactory emf;

    public TransactionManager(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public <R> R doInTransaction(Function<EntityManager, R> transaction) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction txn = em.getTransaction();
        R result = null;
        try {
            txn.begin();

            result = transaction.apply(em);

            txn.commit();
        } catch (Exception e) {
            rollback(txn);
            LOG.error("Something was wrong with transaction!", e);
        } finally {
            em.close();
        }

        return result;
    }

    public void doInTransaction(Consumer<EntityManager> transaction) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction txn = em.getTransaction();
        try {
            txn.begin();

            transaction.accept(em);

            txn.commit();
        } catch (Exception e) {
            rollback(txn);
            LOG.error("Something was wrong with transaction!", e);
        } finally {
            em.close();
        }
    }

    private void rollback(EntityTransaction transaction) {
        if (transaction != null) {
            transaction.rollback();
        }
    }
}
