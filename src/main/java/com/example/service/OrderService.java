package com.example.service;

import com.example.entity.Order;

public interface OrderService extends CrudService<Integer, Order> {
}
