package com.example.service.jpa;

import com.example.entity.Customer;
import com.example.repository.jpa.CustomerRepository;
import com.example.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomerServiceJpaImpl implements CustomerService {

    private CustomerRepository repository;

    @Autowired
    public CustomerServiceJpaImpl(CustomerRepository customerRepository) {
        this.repository = customerRepository;
    }

    @Transactional
    @Override
    public Customer create(Customer entity) {
        return repository.create(entity);
    }

    @Override
    public Customer findByID(Integer id) {
        return repository.findByID(id);
    }

    @Transactional
    @Override
    public Customer update(Customer item) {
        return repository.update(item);
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        repository.delete(id);
    }

    @Transactional
    @Override
    public void delete(Customer item) {
        delete(item.getId());
    }

    @Transactional
    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public Iterable<Customer> findAll() {
        return repository.findAll();
    }
}
