package com.example.service.jpa;

import com.example.entity.Order;
import com.example.repository.jpa.OrderRepository;
import com.example.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderServiceJpaImpl implements OrderService {

    private OrderRepository repository;

    @Autowired
    public OrderServiceJpaImpl(OrderRepository repository) {
        this.repository = repository;
    }

    @Transactional
    @Override
    public Order create(Order entity) {
        return repository.create(entity);
    }

    @Override
    public Order findByID(Integer id) {
        return repository.findByID(id);
    }

    @Transactional
    @Override
    public Order update(Order item) {
        return repository.update(item);
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        repository.delete(id);
    }

    @Transactional
    @Override
    public void delete(Order item) {
        repository.delete(item);
    }

    @Transactional
    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public Iterable<Order> findAll() {
        return repository.findAll();
    }
}
