package com.example.service;

import com.example.entity.Customer;

public interface CustomerService extends CrudService<Integer, Customer> {
}
