package com.example.service.hibernate;

import com.example.entity.Customer;
import com.example.service.CustomerService;

public class CustomerServiceHibernateImpl implements CustomerService {
    @Override
    public Customer create(Customer item) {
        return null;
    }

    @Override
    public Customer findByID(Integer integer) {
        return null;
    }

    @Override
    public Customer update(Customer item) {
        return null;
    }

    @Override
    public void delete(Integer integer) {

    }

    @Override
    public Iterable<Customer> findAll() {
        return null;
    }

    @Override
    public void deleteAll() {

    }

    @Override
    public void delete(Customer item) {

    }
}
