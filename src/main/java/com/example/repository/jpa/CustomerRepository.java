package com.example.repository.jpa;

import com.example.entity.Customer;
import com.example.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
public class CustomerRepository implements CrudRepository<Integer, Customer> {

    @PersistenceContext
    private EntityManager entityManager;

    public CustomerRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Customer create(Customer item) {
        if (item.getId() == null) {
            entityManager.persist(item);
        } else {
            update(item);
        }
        return item;
    }

    @Override
    public Customer findByID(Integer id) {
        return entityManager.find(Customer.class, id);
    }

    @Override
    public Customer update(Customer item) {
        Customer value;
        if (item.getId() != null) {
            value = entityManager.merge(item);
        } else {
            value = create(item);
        }
        return value;
    }

    @Override
    public void delete(Integer id) {
        Customer entity = entityManager.find(Customer.class, id);
        entityManager.remove(entity);
    }

    @Override
    public void delete(Customer item) {
        delete(item.getId());
    }

    @Override
    public void deleteAll() {
        Iterable<Customer> customers = findAll();
        for (Customer customer : customers) {
            entityManager.remove(customer);
        }
    }

    @Override
    public Iterable<Customer> findAll() {
        Query query = entityManager.createQuery("SELECT c FROM Customer c");
        return query.getResultList();
    }
}
