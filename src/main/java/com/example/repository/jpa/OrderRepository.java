package com.example.repository.jpa;

import com.example.entity.Order;
import com.example.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
public class OrderRepository implements CrudRepository<Integer, Order> {

    @PersistenceContext
    private EntityManager entityManager;

    public OrderRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Order create(Order item) {
        if (item.getId() == null) {
            entityManager.persist(item);
        } else {
            update(item);
        }
        return item;
    }

    @Override
    public Order findByID(Integer id) {
        return entityManager.find(Order.class, id);
    }

    @Override
    public Order update(Order item) {
        Order value;
        if (item.getId() != null) {
            value = entityManager.merge(item);
        } else {
            value = create(item);
        }
        return value;
    }

    @Override
    public void delete(Integer id) {
        Order entity = entityManager.find(Order.class, id);
        entityManager.remove(entity);
    }

    @Override
    public void delete(Order item) {
        delete(item.getId());
    }

    @Override
    public void deleteAll() {
        Iterable<Order> customers = findAll();
        for (Order customer : customers) {
            entityManager.remove(customer);
        }
    }

    @Override
    public Iterable<Order> findAll() {
        Query query = entityManager.createQuery("SELECT c FROM Order c");
        return query.getResultList();
    }
}
