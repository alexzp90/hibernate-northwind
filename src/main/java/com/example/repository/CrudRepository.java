package com.example.repository;

public interface CrudRepository<ID extends Number, E> {

    /**
     * Method will create entity or update if it exists.
     *
     * @param item entity to create
     * @return created entity.
     */
    E create(E item);

    E findByID(ID id);

    /**
     * Method will update entity or will create it if ID is null.
     *
     * @param item entity to update
     * @return updated entity.
     */
    E update(E item);

    void delete(ID id);

    void delete(E item);

    void deleteAll();

    Iterable<E> findAll();
}
