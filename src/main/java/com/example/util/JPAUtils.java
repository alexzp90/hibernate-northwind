package com.example.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class JPAUtils {

    private JPAUtils() {
        throw new UnsupportedOperationException();
    }

    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY_INSTANCE =
            Persistence.createEntityManagerFactory("northwind-unit");

    public static EntityManagerFactory getInstance() {
        return ENTITY_MANAGER_FACTORY_INSTANCE;
    }
}
