package com.example.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Ignore("Base class for service layer.")
public abstract class BaseServiceTest {

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public abstract void testCreate();

    @Test
    public abstract void testFindByID();

    @Test
    public abstract void testUpdate();

    @Test
    public abstract void testDeleteByID();

    @Test
    public abstract void testDeleteByEntity();

    @Test
    public abstract void testDeleteAll();

    @Test
    public abstract void testFindAll();
}
