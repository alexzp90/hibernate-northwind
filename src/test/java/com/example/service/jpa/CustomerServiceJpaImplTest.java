package com.example.service.jpa;

import com.example.entity.Customer;
import com.example.service.BaseServiceTest;
import com.example.service.CustomerService;
import com.example.util.CollectionUtils;
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class CustomerServiceJpaImplTest extends BaseServiceTest {
    @Autowired
    private CustomerService customerService;

    @After
    public void tearDown() {
        super.tearDown();
        customerService = null;
    }

    @Override
    public void testCreate() {
        Customer customer = generateCustomer();
        Customer item = customerService.create(customer);

        assertTrue(isEquals(customer, item));
    }

    @Override
    public void testFindByID() {
        Customer customer = generateCustomer();
        Customer newValue = customerService.create(customer);

        Customer value = customerService.findByID(newValue.getId());

        assertTrue(isEquals(newValue, value));
    }

    @Override
    public void testUpdate() {
        Customer customer = generateCustomer();
        Customer newValue = customerService.create(customer);

        newValue.setCompany("new company");

        customerService.update(newValue);

        Customer value = customerService.findByID(newValue.getId());

        assertEquals(newValue.getCompany(), value.getCompany());
    }

    @Override
    public void testDeleteByID() {
        Customer customer = generateCustomer();
        Customer newValue = customerService.create(customer);

        customerService.delete(newValue.getId());

        Customer value = customerService.findByID(newValue.getId());

        assertNull(value);
    }

    @Override
    public void testDeleteByEntity() {
        Customer customer = generateCustomer();
        Customer newValue = customerService.create(customer);

        customerService.delete(newValue);

        Customer value = customerService.findByID(newValue.getId());

        assertNull(value);
    }

    @Override
    public void testDeleteAll() {
        Customer customer = generateCustomer();
        customerService.create(customer);
        Customer customer2 = generateCustomer();
        customerService.create(customer2);

        customerService.deleteAll();

        List<Customer> customers = CollectionUtils.toList(customerService.findAll());
        assertEquals(0, customers.size());
    }

    @Override
    public void testFindAll() {
        Customer customer = generateCustomer();
        customerService.create(customer);
        Customer customer2 = generateCustomer();
        customerService.create(customer2);

        List<Customer> customers = CollectionUtils.toList(customerService.findAll());
        assertEquals(2, customers.size());
    }

    private boolean isEquals(Customer c1, Customer c2) {
        return Objects.equals(c1.getCompany(), c2.getCompany()) &&
                Objects.equals(c1.getCity(), c2.getCity()) &&
                Objects.equals(c1.getAddress(), c2.getAddress()) &&
                Objects.equals(c1.getBusinessPhone(), c2.getBusinessPhone()) &&
                Objects.equals(c1.getFaxPhone(), c2.getFaxPhone()) &&
                Objects.equals(c1.getFirsName(), c2.getFirsName()) &&
                Objects.equals(c1.getLastName(), c2.getLastName()) &&
                Objects.equals(c1.getEmailAddress(), c2.getEmailAddress()) &&
                Objects.equals(c1.getCountryRegion(), c2.getCountryRegion()) &&
                Arrays.equals(c1.getAttachments(), c2.getAttachments()) &&
                Objects.equals(c1.getJobTitle(), c2.getJobTitle()) &&
                Objects.equals(c1.getMobilePhone(), c2.getMobilePhone()) &&
                Objects.equals(c1.getHomePhone(), c2.getHomePhone()) &&
                Objects.equals(c1.getNotes(), c2.getNotes()) &&
                Objects.equals(c1.getStateProvince(), c2.getStateProvince()) &&
                Objects.equals(c1.getWebPage(), c2.getWebPage()) &&
                Objects.equals(c1.getZipPostalCode(), c2.getZipPostalCode());
    }

    private Customer generateCustomer() {
        Customer customer = new Customer();
        customer.setCompany("Elmir");
        customer.setCity("Kharkiv");
        customer.setAddress("Alchevskih str");
        customer.setBusinessPhone("800-22-01");
        customer.setFaxPhone("771088");
        customer.setFirsName("Rudenko");
        customer.setLastName("Oleg");
        customer.setEmailAddress("rudenko@gmail.com");
        customer.setCountryRegion("UA");
        customer.setAttachments("some attachments".getBytes());
        customer.setJobTitle("engineer");
        customer.setMobilePhone("099-555-44-21");
        customer.setHomePhone("88-44-87");
        customer.setNotes("-");
        customer.setStateProvince("Kharkiv");
        customer.setWebPage("-");
        customer.setZipPostalCode("777-88-42");
        return customer;
    }
}