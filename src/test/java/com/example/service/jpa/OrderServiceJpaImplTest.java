package com.example.service.jpa;

import com.example.entity.Customer;
import com.example.entity.Order;
import com.example.service.BaseServiceTest;
import com.example.service.OrderService;
import com.example.util.CollectionUtils;
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class OrderServiceJpaImplTest extends BaseServiceTest {
    @Autowired
    private OrderService orderService;

    @After
    public void tearDown() {
        super.tearDown();
        orderService = null;
    }

    @Override
    public void testCreate() {
        Order order = generateOrder();
        Order item = orderService.create(order);

        assertTrue(isEquals(order, item));
    }

    @Override
    public void testFindByID() {
        Order order = generateOrder();
        Order newValue = orderService.create(order);

        Order value = orderService.findByID(newValue.getId());

        assertTrue(isEquals(newValue, value));
    }

    @Override
    public void testUpdate() {
        Order order = generateOrder();
        Order newValue = orderService.create(order);

        newValue.setShipCity("Paris");

        orderService.update(newValue);

        Order value = orderService.findByID(newValue.getId());

        assertEquals(newValue.getShipCity(), value.getShipCity());
    }

    @Override
    public void testDeleteByID() {
        Order order = generateOrder();
        Order newValue = orderService.create(order);

        orderService.delete(newValue.getId());

        Order value = orderService.findByID(newValue.getId());

        assertNull(value);
    }

    @Override
    public void testDeleteByEntity() {
        Order order = generateOrder();
        Order newValue = orderService.create(order);

        orderService.delete(newValue);

        Order value = orderService.findByID(newValue.getId());

        assertNull(value);
    }

    @Override
    public void testDeleteAll() {
        Order order = generateOrder();
        orderService.create(order);
        Order order2 = generateOrder();
        orderService.create(order2);

        orderService.deleteAll();

        List<Order> orders = CollectionUtils.toList(orderService.findAll());
        assertEquals(0, orders.size());
    }

    @Override
    public void testFindAll() {
        Order order = generateOrder();
        orderService.create(order);
        Order order2 = generateOrder();
        orderService.create(order2);

        List<Order> orders = CollectionUtils.toList(orderService.findAll());
        assertEquals(2, orders.size());
    }

    private boolean isEquals(Order c1, Order c2) {
        return Objects.equals(c1.getEmployeeId(), c2.getEmployeeId()) &&
                Objects.equals(c1.getOrderDate(), c2.getOrderDate()) &&
                Objects.equals(c1.getShippedDate(), c2.getShippedDate()) &&
                Objects.equals(c1.getShipperId(), c2.getShipperId()) &&
                Objects.equals(c1.getShipName(), c2.getShipName()) &&
                Objects.equals(c1.getShipAddress(), c2.getShipAddress()) &&
                Objects.equals(c1.getShipCity(), c2.getShipCity()) &&
                Objects.equals(c1.getShipStateProvince(), c2.getShipStateProvince()) &&
                Objects.equals(c1.getShipZipPostalCode(), c2.getShipZipPostalCode()) &&
                Objects.equals(c1.getShipCountryRegion(), c2.getShipCountryRegion()) &&
                Objects.equals(c1.getShippingFee(), c2.getShippingFee()) &&
                Objects.equals(c1.getTaxes(), c2.getTaxes()) &&
                Objects.equals(c1.getPaymentType(), c2.getPaymentType()) &&
                Objects.equals(c1.getNotes(), c2.getNotes()) &&
                Objects.equals(c1.getPaidDate(), c2.getPaidDate()) &&
                Objects.equals(c1.getTaxRate(), c2.getTaxRate()) &&
                Objects.equals(c1.getTaxStatusId(), c2.getTaxStatusId()) &&
                Objects.equals(c1.getStatusId(), c2.getStatusId());
    }

    private Order generateOrder() {
        Order order = new Order();
        order.setEmployeeId(1L);
        order.setOrderDate(LocalDateTime.of(2019, 6, 10, 0, 0));
        order.setShippedDate(LocalDateTime.of(2019, 6, 15, 0, 0));
        order.setShipperId(1L);
        order.setShipName("Aura");
        order.setShipAddress("USA, NY, 12030");
        order.setShipCity("New York");
        order.setShipStateProvince("-");
        order.setShipZipPostalCode("11111");
        order.setShipCountryRegion("East coast");
        order.setShippingFee(BigDecimal.valueOf(10.5));
        order.setTaxes(BigDecimal.valueOf(0.002));
        order.setPaymentType("credit card");
        order.setNotes("-");
        order.setPaidDate(LocalDateTime.of(2019, 6, 10, 0, 0));
        order.setTaxRate(1.0);
        order.setTaxStatusId((byte) 1);
        order.setStatusId((byte) 1);

        Customer customer = generateCustomer();
        order.setCustomer(customer);

        return order;
    }

    private Customer generateCustomer() {
        Customer customer = new Customer();
        customer.setCompany("Elmir");
        customer.setCity("Kharkiv");
        customer.setAddress("Alchevskih str");
        customer.setBusinessPhone("800-22-01");
        customer.setFaxPhone("771088");
        customer.setFirsName("Rudenko");
        customer.setLastName("Oleg");
        customer.setEmailAddress("rudenko@gmail.com");
        customer.setCountryRegion("UA");
        customer.setAttachments("some attachments".getBytes());
        customer.setJobTitle("engineer");
        customer.setMobilePhone("099-555-44-21");
        customer.setHomePhone("88-44-87");
        customer.setNotes("-");
        customer.setStateProvince("Kharkiv");
        customer.setWebPage("-");
        customer.setZipPostalCode("777-88-42");

        return customer;
    }
}