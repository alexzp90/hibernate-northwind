###Orm test project

This project demonstrates working with 
- JPQL
- Criteria API
- Hibernate alias (HQL)
- jdbcTemplate
- spring data
- Statement/PreparedStatement (самый примитивный способ работы)

Project was written with spring-boot.